<?php

/**
 * @file
 * Contains \Drupal\watchdog_slack\Logger\SlackLog.
 */

namespace Drupal\watchdog_slack\Logger;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\State\State;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\slack\Slack;
use Psr\Log\LoggerInterface;

/**
 * Logs events in the watchdog database table.
 */
class SlackLog implements LoggerInterface {

  use RfcLoggerTrait;
  use DependencySerializationTrait;
  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * The slack service.
   *
   * @var \Drupal\slack\Slack
   */
  protected $slackService;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Stores invalid channels.
   *
   * @var array
   */
  protected $invalidChannels;

  /**
   * Constructs a SlackLog object.
   *
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   * @param \Drupal\slack\Slack $slackService
   *   The slack service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory service.
   */
  public function __construct(LogMessageParserInterface $parser, Slack $slackService, ConfigFactory $configFactory, State $state) {
    $this->parser = $parser;
    $this->slackService = $slackService;
    $this->configFactory = $configFactory;
    $this->state = $state;
    $this->invalidChannels = [
      'watchdog_slack',
      'slack',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function log($level, $message, array $context = []) {
    // Skip specific channels to avoid recursion.
    if (!in_array($context['channel'], $this->invalidChannels)) {

      // Get config.
      $config = $this->configFactory->get('watchdog_slack.settings');

      // Channel is optional
      $channel = $config->get('channel');

      // Get username.
      $username = $config->get('username') ? $config->get('username') : $this->t('Drupal Watchdog');

      // Get truncate and limit per min settings.
      $truncate_length = intval($config->get('truncate_length'));
      $limit_per_minute = intval($config->get('limit_per_minute'));

      // Get severity levels.
      $severity_levels_to_log = $config->get('severity_levels_to_log');
      if (!isset($severity_levels_to_log) || in_array($level, $severity_levels_to_log)) {

        // Bump rate counter.
        $rate_counter = $this->incrementRateCounter();

        // Stop if too many messages sent in the last minute.
        if ($rate_counter > $limit_per_minute) {
          return FALSE;
        }

        $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);

        // Build log message.
        $watchdog_message = (string) $this->t($message, $message_placeholders);
        $watchdog_message = strip_tags($watchdog_message);

        // Build Slack message.
        $slack_message = 'Channel: ' . $context['channel'] . PHP_EOL;
        if ($truncate_length > 0) {
          $slack_message .= 'Message: ' . substr($watchdog_message, 0, $truncate_length) . "..." . PHP_EOL;
        }
        else {
          $slack_message .= 'Message: ' . $watchdog_message . PHP_EOL;
        }
        $slack_message .= 'User Id: ' . $context['uid'] . PHP_EOL;
        $slack_message .= 'Location: ' . $context['request_uri'] . PHP_EOL;
        $slack_message .= 'Referer: ' . $context['referer'] . PHP_EOL;
        $slack_message .= 'Hostname: ' . mb_substr($context['ip'], 0, 128) . PHP_EOL;

        // Send to Slack.
        $result = $this->slackService->sendMessage($slack_message, $channel, (string) $username);
        if (!$result) {
          $this->getLogger('watchdog_slack')
            ->error($this->t("Watchdog wasn't sent to Slack. Please, check Slack module configuration."));
        }
      }
    }
  }

  /**
   * Increment the count of messages sent this minute.
   * If it's a new minute of the hour, reset the counter.
   *
   * @return int
   *   Current count of messages sent in the current minute.
   */
  protected function incrementRateCounter(): int {
    $old_date_minute = $this->state->get('watchdog_slack.rate_counter_minute', 0);
    $date_minute = date('YmdGi');
    $current_count = $this->state->get('watchdog_slack.current_count', 0);

    // New minute means reset counter, else just add one.
    if ($old_date_minute !== $date_minute) {
      $new_count = 1;
    }
    else {
      $new_count = $current_count + 1;
    }

    $this->state->set('watchdog_slack.rate_counter_minute', $date_minute);
    $this->state->set('watchdog_slack.current_count', $new_count);

    return $new_count;
  }

}
