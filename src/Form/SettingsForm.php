<?php

/**
 * @file
 * Contains Drupal\watchdog_slack\Form\SettingsForm.
 * Configures administrative settings for Watchdog to slack.
 */

namespace Drupal\watchdog_slack\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;

/**
 * Class SettingsForm.
 *
 * @package Drupal\watchdog_slack\Form
 *
 * @ingroup slack
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'watchdog_slack_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['watchdog_slack.settings'];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('watchdog_slack.settings');

    $form['channel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slack channel to receive Watchdog notifications.'),
      '#default_value' => $config->get('channel'),
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username for notifications'),
      '#description' => $this->t('How do you like to name your Slack bot?'),
      '#default_value' => $config->get('username') ? $config->get('username') : $this->t('Drupal Watchdog'),
    ];

    $form['truncate_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Truncate Length for notifications'),
      '#description' => $this->t('Stack Traces are long, and are hard to read in Slack.  Suggest a truncate of 75.'),
      '#min' => 50,
      '#default_value' => $config->get('truncate_length') ? $config->get('truncate_length') : 75,
    ];

    $form['limit_per_minute'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit of Messages per Run'),
      '#description' => $this->t('Limit how many messages to send per minute.  Any message after this threshold will not be sent to Slack.  A high number will cause blocking and likely crash a Slack client.'),
      '#min' => 5,
      '#default_value' => $config->get('limit_per_minute') ? $config->get('limit_per_minute') : 5,
    ];

    $severity_levels_to_log = RfcLogLevel::getLevels();
    $form['severity_levels_to_log'] = [
      '#title' => $this->t('Severity levels to log'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 8,
      '#options' => $severity_levels_to_log,
      '#default_value' => $config->get('severity_levels_to_log'),
      '#description' => $this->t('Use caution with levels below Warning, as this makes Slack very verbose.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('watchdog_slack.settings');
    $config
      ->set('channel', $form_state->getValue('channel'))
      ->set('username', $form_state->getValue('username'))
      ->set('truncate_length', $form_state->getValue('truncate_length'))
      ->set('limit_per_minute', $form_state->getValue('limit_per_minute'))
      ->set('severity_levels_to_log', $form_state->getValue('severity_levels_to_log'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
